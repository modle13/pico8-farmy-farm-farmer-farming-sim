function control_player()
    if(inv_itm.on or qsts.on or mail.on or shop.on)return
    if (btnp(4)) check_npc_interaction() return
    if (btn(0)) plr.dx-=0.075 plr.flip_x=true
    if (btn(1)) plr.dx+=0.075 plr.flip_x=false
    if (btn(2)) plr.dy-=0.075
    if (btn(3)) plr.dy+=0.075
    plr:move()
    if cur_npc!=nil then
        local dist=pythag(cur_npc.x-plr.x+plr.w,cur_npc.y-plr.y+plr.h)
        if(dist>0.9)cur_npc.dialog:toggle(false)
    end
end
function check_npc_interaction()
    if(chktm(last_check)<0.3)return
    last_check=time()
    local x,y=flr(plr.x),flr(plr.y)
    local pos=prpos(x,y)
    local res=check_for_resource(x,y)
    if(res!=nil) res:process() return
    local npc=npc_map_crds[pos]
    if(npc!=nil and npc.name!='event')cur_npc=npc npc:process() return
    local cur=mget(x,y)
    if(contains(shop_ids,cur))shop:toggle(true)return
    if(cur==127)qsts:toggle(true)return
    if(not have_can and pos=='43,8') spawn_gem(x,y,253) have_can=true can_spr=253 mset(70,5,205) parse_map_cells()
    if cur==197 or cur==198 then
        gen_prd(x+0.5,y+0.5,rnd(prd_ids),inv_itm,1)
        mset(spot.x,spot.y,0)
        spot=nil
        parse_map_cells()
        sfx(51)
    end
end
function draw_tool(x,y)
    local toolx,tooly,flip=x or plr.x*8+2,y or plr.y*8-4,false
    if(x==nil and plr.flip_x)toolx-=12 flip=true
    if(dr_tl_cnt<10)spr(tool_spr,toolx,tooly,1,1,flip)dr_tl_cnt+=1
end
