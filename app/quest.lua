function quest:new(name)
    local o={
        data={},
        name=name or 'quests',
        cur=nil,
        on=false,
        tgl_tm=0,
        max_qsts=5,
        w=12,
        cmpl=0,
        earned=0,
    }
    make_obj(o,self)
    return o
end
function quest:create_entry(h,items,tgt)
    local a=actor:new(-1,2,0,'')
    a.bkng=true
    a.bck_w,a.bck_h=(self.w+2)*8,(h+2)*8
    self.cur=a a.selected=true
    a.sub_data={}
    a.cmpl=false
    a.coin=actor:new(80,2.25,0,'coin',0.2,4)
    a.coin.clr,a.coin.value=7,0
    for k,v in pairs(items) do
        self:make_sub_item(a,k,v,tgt)
        a.name=k
        if(tgt=='shop')a.coin.value=v
        if tgt==nil then
            if(rnd(1)<0.05)a.coin.value*=3 a.coin.bns=true
        end
    end
    add(self.data,a,1)
    set_ix(self.data,self.cur)
    self:adj_y()
    return a
end
function quest:make_sub_item(prnt,k,v,tgt)
    local sub_a=actor:new(k,6.5+#prnt.sub_data*2,0,prd_names[k])
    sub_a.count=v
    prnt.coin.value+=v*flr(rrng(3,1.5))
    sub_a.bkng=true
    sub_a.label=tgt or 'qst_itm'
    sub_a.cnt_clr=7
    add(prnt.sub_data,sub_a)
end
function quest:update()
    if not self.on then
        local dta=self.data
        if self.name=='quests' and dta!=nil then
            for ea in all(dta) do
                if(ea.cmpl)del(dta,ea)
            end
            if(#dta<self.max_qsts)self:gen()
        end
        return
    end
    self:set_text_clr()
    if (btnp(5) and self.on)self:toggle(false) return
    if btnp(4) then
        if(self.name=='quests')self:check_nums()
        if(self.name=='shop' and chktm(self.tgl_tm)>0.25)self:buy()
    end
    if (btnp(2) or btnp(3)) self:cycle(btnp(2))
end
function quest:draw()
    if (not self.on) return
    for ea in all(self.data) do
        self:draw_entry(ea)
    end
    if(self.name=='quests')showtext(self.name..',completed:'..self.cmpl..',earned:'..self.earned,64,-6)
end
function quest:set_text_clr()
    for i,v in pairs(self.data) do
        if(self.name=='shop' and v.coin.value>coins)v.coin.clr=8 else v.coin.clr=7
        for j,w in pairs(v.sub_data) do
            local _,itm=inv_itm:get(w.name)
            local cnt=0
            if (itm!=nil) cnt=itm.count
            if (cnt!=nil and cnt>=w.count) w.cnt_clr=7 else w.cnt_clr=8
        end
    end
end
function quest:gen()
    local entries=flr(rrng(2,3))
    local new_qst={}
    local cnt=0
    while cnt<rrng(8,3) do
        local nxt_itm=rnd(prd_ids)
        local nxt_val=flr(rrng(2,2))
        if new_qst[nxt_itm]==nil then
            new_qst[nxt_itm]=nxt_val
            cnt+=nxt_val
        end
    end
    if(self.cmpl==0 and #self.data==0) self:create_entry(0,{[72]=1}) else self:create_entry(0,new_qst)
end
function quest:check_nums()
    if(chktm(self.tgl_tm)<0.25) return
    if(self.cur.cmpl) return
    local rdy=true
    for ea in all(self.cur.sub_data)do
        local _,itm=inv_itm:get(ea.name)
        if itm==nil or ea.count>itm.count then
            rdy=false
            break
        end
    end
    if (rdy)self:complete()
end
function quest:buy()
    local amt=self.cur.coin.value
    if amt<=coins then
        local a=inv_gem:add(self.cur.name)
        a.selected=false
        coins-=amt
        sfx(45)
    end
end
function quest:complete()
    local cns=self.cur.coin.value
    coins+=cns
    coins=(min(30000,coins))
    self.cur.cmpl=true
    for ea in all(self.cur.sub_data)do
        inv_itm:adj(ea.name,-ea.count)
    end
    self.cmpl+=1
    self.earned+=cns
    self.earned=(min(30000,self.earned))
    sfx(45)
end
function quest:cycle(rev)
    sfx(44)
    self.cur.selected=false
    self.cur=getnext(self.cur,self.data,rev)
    self.cur.selected=true
    self:adj_y()
end
function quest:adj_y()
    for i,v in pairs(self.data) do
        v.y=7+(v.ix-self.cur.ix)*4
        v.coin.y=v.y+1
        for j,w in pairs(v.sub_data) do
            w.y=v.y+1
        end
    end
end
function quest:toggle(st)
    if(st!=nil)self.on=st else self.on=not self.on
    self.tgl_tm=time()
    chst_snd(self)
end
function quest:draw_entry(a)
    a:draw()
    local x,y=a.x,a.y+0.75
    if(a.cmpl)print('complete!',(x+1)*8,(y)*8,11) return
    if self.name=='quests' or self.name=='shop' then
        local cn=a.coin
        cn:move()
        cn:draw()
        print(cn.value,x*8+8,(y)*8,cn.clr)
        if(cn.bns)print("x3!",x*8+8,(y+0.75)*8,11)
    end
    foreach(a.sub_data,draw_sub_entry)
end
function draw_sub_entry(a) a:draw() end
function create_trd_inv(entry)
    local sub=inv:new()
    sub.x=entry.x+5.5+3*#entry.sub_data
    sub.y=entry.y+3
    sub.h=5
    add(entry.sub_data,sub)
    return sub
end
function make_shop()
    local shop=quest:new('shop')
    shop.w=11
    shop:create_entry(0,{[200]=250},'shop')
    shop:create_entry(0,{[202]=700},'shop')
    return shop
end
