function gem:new(x,y,txt,spr_id)
    local o=npc:new(x,y,txt,'gem',spr_id)
    o.on=false
    make_obj(o,self)
    return o
end
function gem:update()end
function gem:draw()if(self.on)spr(self.sprite_id,self.x*8,self.y*8-2-sin(frm_ref/60)*2)end
function gem:process(ld)
    if(not gems_on)return
    local _,a=inv_gem:get(prd_names[self.sprite_id])
    local _,cn=inv_gem:get('can')
    if cn!=nil and self.sprite_id==218 then
        shatter_gem(self)
    elseif red[2] and not red[3] then
        turn_red(self)
    else
        local cont=false
        if not ld then
            if(not self.on and a!=nil and a.count>0)a.count-=1 cont=true
            if(self.on)inv_gem:add(self.sprite_id)cont=true
            if(not cont)return
        end
        self.on=not self.on
    end
    self.x,self.y=self.st_x,self.st_y
    for ea in all(self.trg) do
        set_triggers(ea,self.clr,self.on)
    end
end
function make_gems()
    for ea in all(get_map_cells('gem',ld_ids('200,202'))) do
        local x,y=ea.x,ea.y
        local a=gem:new(x,y,'',ea.sprite_id)
        a.mspr=ea.sprite_id-1
        setm(a,0)
        a.st_x,a.st_y=x,y
        add(gems,a)
    end
    for ea in all(split(gem_data,'|')) do
        local s=split(ea,'$')
        local cr,tg=split(s[1]),split(s[2])
        local clr=cr[1]
        local g=npc_map_crds[prpos(cr[2],cr[3])]
        g.sb=npc_map_crds[prpos(cr[4],cr[5])]
        g.clr,g.trg=clr,spl_crds(tg)
    end
end
function trigger_absorb(r)
    if(r.absorbed)return
    r.absorbed,r.growing=true,false
    reset_res(r,r.ld_id)
    local a=actor:new(-1,r.x,r.y,'frc')
    a.xt,a.yt=57.5,1.5
    add(misc_actors,a)
    absorbed+=1
    shkf=0
    if(absorbed==1)go_tm=time()foreach(resources,trigger_absorb)gem_spread()
end
function gem_spread()
    send_mail(97)
    send_mail(98)
    rmap("42,4,|43,4,|42,5,|43,5,|38,6,|39,6,|40,6,|41,6,|42,6,|43,6,|44,6,|45,6,|46,6,|47,6,|40,7,|41,7,|42,7,|43,7,|44,7,|45,7,|40,8,|41,8,|42,8,|43,8,|44,8,|45,8,|40,9,|41,9,|42,9,|43,9,|44,9,|45,9,|40,10,|41,10,|42,10,|43,10,|44,10,|45,10,|40,11,|41,11,|42,11,|43,11,|44,11,|45,11,|41,12,|42,12,|43,12,|44,12,|48,22,187|49,22,187|48,23,187|49,23,187|48,24,187|49,24,187|49,3,187|49,4,187")
    for ea in all(gems) do
        if(ea.clr!='rg')ea:process() shkf=0
        if(absorbed<1)return
    end
end
function turn_red(a)
    if(red[3] or a.clr=='rg')return
    send_mail(99)
    a.ogspr,a.ogclr,a.ogst=a.sprite_id,a.clr,a.on
    a.sb.ogspr,a.sb.ogclr,a.sb.ogst=a.sb.sprite_id,a.sb.clr,a.sb.on
    a.sprite_id,a.clr,a.on,a.sb.sprite_id,a.sb.clr,a.sb.on=218,'rg',true,218,'rg',true
    corrupted+=1
end
function set_triggers(a,clr,st,shatter)
    local r=res_crds[prpos(a.x,a.y)]
    if(r==nil)return
    if(shatter)r.absorbed=false reset_res(r,r.ld_id)r.bst=true r.grow_tick=time()
    r[clr]=st
end
function shatter_gem(a)
    if(a.shatter)return
    sfx(57)
    inv_gem:adj('red',-1)
    send_mail(96)
    send_mail(95)
    rmap("58,1,204|59,1,11|60,1,12|61,1,13|58,2,26|59,2,27|60,2,28|61,2,29|58,3,42|59,3,43|60,3,44|61,3,45|58,4,58|59,4,59|60,4,60|61,4,61|58,5,203|59,5,203|60,5,203|61,5,203|57,1,217")
    a.sprite_id,a.clr,a.on=a.ogspr,a.ogclr,a.ogst
    a.sb.sprite_id,a.sb.clr,a.sb.on=a.sb.ogspr,a.sb.ogclr,a.sb.ogst
    a.shatter,a.sb.shatter=true,true
    corrupted-=1
    for ea in all(a.trg) do
        set_triggers(ea,'rg',false,true)
        set_triggers(ea,a.clr,a.on)
    end
    dr_tl_cnt,tool_spr,red[3],misc_actors=0,can_spr,true,{}
end
