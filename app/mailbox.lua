function mail_add(subj,cont)
    sfx(54)
    local a=dialog:new(subj)
    a.x=2
    a.on=true
    a.button=false
    a.selected=true
    a.content=cont
    a.extr=158
    a.unread=true
    a.id=#mail.data
    mail.cur=a
    add(mail.data,a,1)
    mail_adjust_y()
    mail.unread=mail.unread or a.unread
    return a
end
function mail_draw()
    if (not mail.on) return
    mail.unread=false
    foreach(mail.data,draw_mail)
    showtext(mail.name,64,-4)
end
function mail_cycle(rev)
    mail.cur.selected=false
    mail.cur=getnext(mail.cur,mail.data,rev)
    mail.cur.selected=true
    mail_adjust_y()
    sfx(44)
end
function mail_adjust_y()
    local j,cur=contains_attr(mail.data,'selected',true)
    for i,v in pairs(mail.data) do
        v.y=(8+(#v.txt+2)*(i-j))*8
    end
end
function mail_update()
    if(not mail.on)return
    if btnp(5) and mail.on then
        toggle_mail() mail.dialog.on=false mail.tgl_tm=0
    end
    if #mail.data>0 then
        if (btnp(2) or btnp(3))mail_cycle(btnp(2))
        if btnp(4) then
            if (not mail.dialog.on and chktm(mail.tgl_tm)>0.25)mail_tgl_msg(true) return
            if mail.dialog.on then
                if(mail.dialog.fullread)mail_tgl_msg(false) return
                if(not mail.dialog.fullread)mail.dialog.fullread=true mail.tgl_tm=time()
            end
        end
    end
end
function mail_tgl_msg(st)
    sfx(45)
    mail.dialog:toggle(st)
    mail_adjust_y()
    if mail.dialog.on then
        mail.dialog.txt=mail.cur.content
        mail.cur.unread=false
        mail.cur.extr=159
    else
        mail.tgl_tm=0
        mail.dialog.txt=nil
    end
end
function draw_mail(a)
    if(a.unread)mail.unread=true
    a:draw()
    mail.dialog:draw()
end
function toggle_mail()
    mail.on=not mail.on
    if(mail.on and #mail.data==0)mail.on=false
    chst_snd(mail)
    mail.tgl_tm=time()
    mail_adjust_y()
end
function ld_mail()
    for ea in all(split(mail_data,'|')) do
        local spl=split(ea,';')
        mail_triggers[spl[1]]={spl[2],spl[3]}
    end
end
function make_mailboxes()
    for ea in all(get_map_cells('mail',{[111]=1})) do
        local a=npc:new(ea.x,ea.y,'txt','mail')
        a.process=toggle_mail
        add(mailboxes,a)
    end
end
