function update_prd(a) a:update() a:move() end
function prd:new(sprite_id,x,y,name,inv,frm)
    local o=actor:new(sprite_id,x,y,name,nil,frm or 1)
    make_obj(o,self)
    o.draw,o.move=actor.draw,actor.move
    o.move=actor.move
    o.spr_type='prd'
    o.spwn_tm=time()
    o.spwn_done=false
    o.inv=inv or inv_itm
    return o
end
function prd:update()
    if(self.spwn_done and chktm(self.spwn_tm)<0.5)return
    local rdy=self.spwn_done
    if(not rdy)self.spwn_tm=time() else self.xt,self.yt=plr.x,plr.y
    local dist=pythag(self.xt-self.x,self.yt-self.y)
    if(rdy and dist>8)return
    set_tgt_dir(self)
    if(dist<1 and not rdy)self.spwn_done=true
    if dist<0.2 and rdy then
        if(self.inv=='coin')coins+=1 coins=(min(30000,coins)) sfx(49) else self.inv:add(self.sprite_id)
        if(self.sprite_id==218)send_mail(94)red[2]=true
        if(contains(ld_ids('200,202,218,253'),self.sprite_id))sfx(56)
        del(prds,self)
    end
end
function make_prd(src_spr_id,x,y)
    local prd_name=prd_map[src_spr_id]
    local cnt=prd_cnt[prd_name]
    if(rnd(1)<0.01)cnt+=rnd(20)
    for i=0,rrng(cnt,cnt/2) do
        gen_prd(x,y,prd_name)
        prds_since_gem+=1
    end
    check_extras(x,y,src_spr_id)
end
function gen_prd(x,y,id,inv,frm,xo)
    local prd=prd:new(id,x,y,prd_names[id],inv,frm)
    prd.xt,prd.yt=x+rrng(3,-1.5)+(xo or 0),y+rrng(3,-1.5)
    add(prds,prd)
    return prd
end
function check_extras(x,y,src_spr_id)
    if prds_since_gem>100 and (time()-last_gem)>300 and contains(crop_end_ids,src_spr_id) and gems_on then
        spawn_gem(x,y,200)
        last_gem,prds_since_gem=time(),0
    end
    if(rnd(1)<0.02) for n=1,rnd(50) do gen_prd(x,y,80,'coin',4,2) end
end
function spawn_gem(x,y,id)
    gen_prd(x,y,id,inv_gem,1,2)
end
