function inv:new(name,on)
    local o={
        data={},
        name=name or 'inventory',
        cur=nil,
        on=on or false,
        x=1,
        y=7,
        h=20,
        sort_by=1,
    }
    make_obj(o,self)
    o.toast=actor:new(0,o.x+7,o.y-2.5,'')
    o.toast.tm=time()-5
    return o
end
function inv:add(id,amt)
    sfx(48)
    local amt=amt or 1
    local name=prd_names[id]
    local a=self:adj(name,amt)
    if(a!=nil) return a
    local a=actor:new(id,self.x,0,name)
    a.bkng=true
    a.count,a.label,a.selected=amt,'inv_itm',true
    add(self.data,a)
    self.cur=a
    self:update_ui(a)
    return a
end
function inv:adj(nm,amt)
    local _,a=self:get(nm)
    if(a!=nil)a.count+=amt a.count=min(a.count,30000) self:update_ui(a) return a
end
function inv:update_ui(a)self:sort()self:sort()self:show_toast(a)end
function inv:show_toast(a)
    local tst=self.toast
    tst.tm,tst.name,tst.sprite_id,tst.count=time(),a.name,a.sprite_id,a.count
end
function inv:adjust_y()
    local j,cur=self:get(self.cur.name)
    if(j==nil)return
    for i,v in pairs(self.data) do v.y=self.y+(i-j)*2 end
end
function inv:sort()
    local sortf=sorts[self.sort_by]
    for i=1,2 do
        sort(self.data,sortf,sortf=='count')
        self:adjust_y()
        set_ix(self.data,self.cur)
    end
end
function inv:draw()
    if (not qsts.enabled) self:draw_entries()
    if self.on then
        showtext(self.name..',sort:'..sorts[self.sort_by],64,-6)
        if(self.cur!=nil)showtext(self.cur.name,64,42)
    end
end
function inv:get(nm)return contains_attr(self.data,'name',nm)end
function inv:draw_entries(a)
    for ea in all(self.data) do
        if self.name=='inventory' then
            local dr_spr,y=0,ea.y
            if(ea.y<2)dr_spr=76
            if(ea.y>14)dr_spr=92 y-=0.6
            if(dr_spr==0)ea:draw() else draw_arrow(dr_spr,(ea.x-0.5)*8,y*8)
        else
            local _,itm=self:get(ea.name)
            if(itm!=nil and itm.count>=ea.count)ea.cnt_clr=7 else ea.cnt_clr=8
            ea:draw()
            if(ea.count==0)del(self.data,ea)
        end
    end
    if(chktm(self.toast.tm)<2)showtext(self.toast.count,64,32) self.toast:draw()
end
function inv:update()
    if(#self.data==0 or not self.on) return
    if(btnp(2) or btnp(3)) self:cycle()
    if(btnp(4)) self.sort_by=next(sorts, self.sort_by) or 1 self:sort()
end
function inv:cycle()
    if(not self.on)return
    self.cur.selected=false
    self.cur=getnext(self.cur,self.data,btnp(2))
    self.cur.selected=true
    self:adjust_y() sfx(44)
end
function check_menu_ops()
    if(btn(6)) poke(0x5f30,1)toggle_inv()
    if(btnp(5) and inv_itm.on)toggle_inv()
end
function toggle_inv()
    if(qsts.on or mail.on) return
    inv_itm.on=not inv_itm.on
end
