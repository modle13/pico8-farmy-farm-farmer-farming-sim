function next_is_solid(actor,dx,dy)
    local chx,chy=getvec(actor)
    return solid_area(chx+dx,chy+dy,actor.w,actor.h)
end
function solid_area(x,y,w,h)
    return solid(x-w,y-h) or solid(x+w,y-h) or solid(x-w,y+h) or solid(x+w,y+h)
end
function solid(x,y)
    return fget(mget(x,y),1)
end
