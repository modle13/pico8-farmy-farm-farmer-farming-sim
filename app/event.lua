function update_events()
    local pos=prpos(flr(plr.x),flr(plr.y))
    if(flood_on and not flood_done)flood()
    if(pos=='9,28' and not gems_on)event_gems()
    if(pos=='17,5' and not mail['17,5'])flood_on=true
    if(pos=='57,1' and not red[1])spawn_gem(plr.x,plr.y,218)sfx(55) red[1]=true rmap("29,2,186|30,2,186|31,2,186|32,2,186|33,2,186")
    for k,v in pairs(mail_triggers) do
        if(pos==k)send_mail(k) break
    end
end
function send_mail(k)
    if(not mail_triggers[k])return
    mail_add({mail_triggers[k][1]},split(mail_triggers[k][2]))mail_triggers[k]=nil
end
function flood()
    plr.x,plr.y=17.5,6
    if(game_state<2 or frm_ref%5!=0)return
    flood_cur_x-=1
    for y=flood_cur_y,flood_end_y do
        mset(flood_cur_x,y,152)
    end
    shk()
    parse_map_cells()
    if(flood_cur_x==flood_end_x)flood_done=true
end
function event_gems()
    sfx(33)
    for ea in all(gems) do
        local cur=mget(ea.x,ea.y)
        if cur==197 or cur==198 then
            spot=nil
            parse_map_cells()
        end
        setm(ea,ea.mspr)
    end
    gems_on=true
end
