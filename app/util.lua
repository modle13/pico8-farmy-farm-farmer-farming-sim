function manage_frm()
    frm_ref+=1 frm_ref%=60
end
function update_reference_sprites()
    ref_wave.frm+=0.0625 ref_wave.frm%=ref_wave.frms
end
function draw_actor(a)
    a:draw()
end
function move_actor(a)
    a:move()
end
function getnext(start,coll,rev)
    local starti=rev and #coll or 1
    local endi=rev and 1 or #coll
    local iter=rev and -1 or 1
    local get_next=nil
    local result=nil
    for i=starti,endi,iter do
        if(result!=nil)break
        if(get_next)result=coll[i]
        get_next=start==coll[i]
    end
    if result!=nil then
        return result
    else
        return coll[starti]
    end
end
function contains(set,key)
    return set~=nil and set[key]~=nil
end
function prpos(x,y)
    return x..','..y
end
function getvec(a)
    return a.x,a.y
end
function contains_attr(set,attr,val)
    for i,v in pairs(set) do
        if (v[attr]==val) return i,v or -1,{}
    end
end
function pythag(a,b)
    return sqrt(a^2+b^2)
end
function sort(a,fld,isint)
    for i=1,#a do
        local j=i
        while j>1 do
            local fld1,fld2=a[j-1][fld],a[j][fld]
            if(isint)fld1,fld2=tonum(fld1),tonum(fld2)
            if(fld1>fld2)local tomove=a[j]del(a,tomove)add(a,tomove,j-1)
            j=j-2
        end
    end
end
function make_obj(o,self)setmetatable(o, self) self.__index=self
end
function chktm(tm)
    return abs(tm-max(time(),tm))
end
function search(coll,st)
    for ea in all(coll) do
        if(st==ea)return true
    end
    return false
end
function spl_crds(dta)
    local out={}
    for i,v in pairs(dta) do
        if(i%2!=0)add(out,{x=v,y=dta[i+1]})
    end
    return out
end
function set_ix(dta,cur)
    local ret=cur
    for i,v in pairs(dta) do
        v.ix=i
        if(cur.cmpl)ret=v v.selected=true
    end
    return ret
end
function rrng(n,o)return rnd(n)+o end
function set_tgt_dir(a)
    local spd=a.spd or 0.1
    if(a.x<a.xt)a.dx+=spd a.flip_x=false
    if(a.x>a.xt)a.dx+=-spd a.flip_x=true
    if(a.y<a.yt)a.dy+=spd
    if(a.y>a.yt)a.dy+=-spd
end
