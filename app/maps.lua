function parse_map_cells()
    wave_cells=get_map_cells('wave',wave_sprite_ids)
    ref_wave=wave_cells[1]
    if(game_state>1)return
    crtr_cells=get_map_cells('crtr',crtr_load_ids)
    res_map_crds=get_map_cells('res',res_ids)
end
function get_map_cells(name,filter_ids,xl,yl)
    local map_cells={}
    for y=0,yl or 31 do
        for x=0,xl or 127 do
            spr_id=mget(x,y)
            if contains(filter_ids,spr_id) then
                add(map_cells,actor:new(spr_id,x,y,name))
            end
        end
    end
    return map_cells
end
function update_map_cells(cell,frm)
    if (cell.name=='wave') frm=ref_wave.frm
    setm(cell,wv_spr_map[cell.sprite_id]+frm)
end
function setm(a,id)mset(a.x,a.y,id or a.sprite_id) end
function rmap(dt)
    for ea in all(split(dt,'|')) do
        local s=split(ea)
        local x,y,sp=s[1],s[2],s[3]
        for ea in all(wave_cells) do
            if(ea.x==x and ea.y==y)del(wave_cells,ea)
        end
        mset(x,y,sp)
    end
end
