function ld_ids(data)
    local ids,cur={},1
    local spl=split(data,'|')
    local vals=split(spl[2]) or {}
    for ea in all(split(spl[1])) do
        ids[ea]=vals[cur] or 1
        cur+=1
    end
    return ids
end
wave_sprite_ids=ld_ids('136,137,152,153,168,169,184,185,197,198,234,235')
wv_spr_map=ld_ids('136,137,152,153,168,169,184,185,197,198,234,235|136,136,152,152,168,168,184,184,197,197,234,234')
res_ids=ld_ids('98,99,100,101,102,103,114,115,119,131,135,147,151,163,167,179,183,194,195,211,227')
crop_proc_ids=split('100,101,102,103')
crop_start_id=100
res_start_id=124
crop_data_in=[[
99$124,96,97,98,99
|115$124,112,113,114,115
|119$116,117,118,119
|131$124,128,129,130,131
|135$132,133,134,135
|147$144,145,146,147
|151$148,149,150,151
|163$160,161,162,163
|167$164,165,166,167
|179$176,177,178,179
|183$180,181,182,183
|195$124,192,193,194,195
|211$208,209,210,211
|227$124,224,225,226,227
]]
crop_grow_ids={}
for ea in all(split(crop_data_in,'|')) do
    spl2=split(ea,'$')
    crop_grow_ids[spl2[1]]=split(spl2[2])
end
multi_hp_crops=ld_ids('99,115,131,195,227')
fruit_crops=ld_ids('99,115,195')
crop_end_ids=ld_ids('119,135,151,167,183')
res_load_ids=ld_ids('98,99,114,115,119,131,135,147,151,163,167,179,183,194,195,211,227')
prd_map=ld_ids('98,99,114,115,119,131,135,147,151,163,167,179,183,194,195,211,227|74,72,74,73,88,75,89,105,90,106,91,120,104,74,121,107,74')
prd_ids=split('72,73,74,75,88,89,90,91,104,105,106,107,120,121')
prd_cnt=ld_ids('72,73,74,75,88,89,90,91,104,105,106,107,120,121,200,202|1,1,2,1,2,1,2,1,1,1,1,1,1,2,100,150')
prd_names=ld_ids('72,73,74,75,88,89,90,91,104,105,106,107,120,121,80,200,202,218,253|apple,berry,wood,mushroom,corn,strawberry,tomato,pepper,pumpkin,honey,milk,egg,wool,blueberry,coin,green,yellow,red,can')
crtr_load_ids=ld_ids('68,70,84,86,240,242,220,236,228,212,214')
crtr_map=ld_ids('68,70,84,86,240,242,220,236,228,212,214|bee,chicken,cow,sheep,bee,chicken,cow,sheep,mouse,cat,bird')
shop_ids=ld_ids('238,239,254,255')
unlock_data=[[
94,17,3,wall,5,wall,0,1
|94,9,26,wall,300,wall,0,1
|95,35,24,wall,10,wall,-1,0
|95,39,26,wall,800,wall,1,0
|227,4,8,tree,15,res,0,0
|227,8,8,tree,75,res,0,0
|99,4,16,apple tree,15,res,0,0
|99,7,14,apple tree,75,res,0,0
|115,10,10,berry bush,15,res,0,0
|115,14,9,berry bush,75,res,0,0
|195,11,15,blueberry bush,15,res,0,0
|195,15,17,blueberry bush,75,res,0,0
|131,29,13,giant mushroom,15,res,0,0
|131,30,15,giant mushroom,75,res,0,0
|147,2,23,beehive,15,res,0,0
|147,4,23,beehive,75,res,0,0
|163,13,23,milk,15,res,0,0
|163,15,23,milk,75,res,0,0
|179,21,23,wool,15,res,0,0
|179,23,23,wool,75,res,0,0
|211,29,23,egg,15,res,0,0
|211,31,23,egg,75,res,0,0
|119,23,6,corn,15,res,0,0
|119,23,7,corn,75,res,0,0
|135,23,9,strawberry,15,res,0,0
|135,23,10,strawberry,75,res,0,0
|151,23,12,tomato,15,res,0,0
|151,23,13,tomato,75,res,0,0
|167,23,15,pepper,15,res,0,0
|167,23,16,pepper,75,res,0,0
|183,23,18,pumpkin,15,res,0,0
|183,23,19,pumpkin,75,res,0,0
]]
sign_data=[[
2,1,welcome to farmyfarm!,
|4,1,farmyfarm is a happy place!$$trust me!,
|12,1,a mushroom!,
|14,1,poor mushroom :($$but don't worry$$it'll grow back,
|7,3,chop down this tree$to collect wood!$$it's fine!$$promise!,
|9,3,feels great right?$$stuff!$$press the pause button$to see inventory details$and set the sort order,
|16,3,gain access to new$resources or areas$by purchasing these [?],
|12,3,sell your spoils$at these boxes,
|14,3,coins!$$$check the quest box often!$$more quests will appear$after a while.,
|20,6,these are crop plots$$interact with them a few$times to plant crops!$$then wait!,
|7,22,look! buzzy bees!$$$isn't that nice?$$please be kind to them,
|17,22,moooooooooooooooooooooo$ooooooooooooooooooooooo$ooooooooooooooooooooooo$$$moo.,
|25,22,they're a bit...$$baaaaah-shful,
|33,22,hope you brought a snack$$they're a little peckish,
|10,29,welcome to the gem shoppe!$$this peculiar valley has the$unusual trait of focusing$your labors into magical$energy crystals! then we dig$them up and sell them back$to you! sounds fair right!?,
|10,30,gem characteristics:$$yellow: auto-plant/harvest$$green: speed-grow$$there are rumors of$a third gem...$$a red gem...$$we would very much like to$study a red gem should you$find one,
|39,28,ah the fresh smell$of the ocean$$beware the crabs...$$they talk too much and $are always having dance$parties.$$can't even hear myself think,
|37,23,jerry over here won't$stop beat-boxing,
|39,24,un tss un tss un tss$bwaaaaaaaaaaaaaaaaaa$un tss un tss un tss$bwaaaaaaaaaaaaaaaaaa,
|37,27,i pinch?,
|38,29,birds are the worst,
|43,26,yarrr! this be a message$in a bottle!$$we be the bridge pirates!$$boats be'n fer scalawags!$not'n fer the likes o'$brownboard the builder and$his merry trades!
|58,24,oh no!,
|61,25,i wonder what happened...,
|59,17,looks like they've been$here a long time...,
|61,16,all dusty and cracked$$maybe they were...$$the bridge pirates!$$yarrr! could be treasure$afoot me hearties!,
|60,12,...this one looks a lot$fresher...$$still... glisteny...$$ew.$$...hope they didn't get$all the treasure!,
|62,1,here lies cliff th' pirate$$cliff fell off a cliff.$$but he was fine!$$then he found...$$a cave...$$then he was less fine.,
]]
gem_data=[[
yg,6,9,7,9$4,8,5,7,5,10,8,8
|gg,7,9,6,9$4,8,5,7,5,10,8,8
|yg,5,15,6,15$4,13,4,16,6,17,7,14
|gg,6,15,5,15$4,13,4,16,6,17,7,14
|yg,11,8,12,8$10,10,11,6,12,11,14,9
|gg,12,8,11,8$10,10,11,6,12,11,14,9
|yg,12,17,13,17$10,18,11,15,14,15,15,17
|gg,13,17,12,17$10,18,11,15,14,15,15,17
|yg,28,15,29,15$28,13,28,17,29,13,30,15
|gg,29,15,28,15$28,13,28,17,29,13,30,15
|yg,20,7,25,7$22,6,23,6,22,7,23,7
|gg,25,7,20,7$22,6,23,6,22,7,23,7
|yg,20,10,25,10$22,9,23,9,22,10,23,10
|gg,25,10,20,10$22,9,23,9,22,10,23,10
|yg,20,13,25,13$22,12,23,12,22,13,23,13
|gg,25,13,20,13$22,12,23,12,22,13,23,13
|yg,20,16,25,16$22,15,23,15,22,16,23,16
|gg,25,16,20,16$22,15,23,15,22,16,23,16
|yg,20,19,25,19$22,18,23,18,22,19,23,19
|gg,25,19,20,19$22,18,23,18,22,19,23,19
|yg,2,22,4,22$2,23,4,23,2,25,4,25
|gg,4,22,2,22$2,23,4,23,2,25,4,25
|yg,13,22,15,22$13,23,15,23,13,25,15,25
|gg,15,22,13,22$13,23,15,23,13,25,15,25
|yg,21,22,23,22$21,23,23,23,21,25,23,25
|gg,23,22,21,22$21,23,23,23,21,25,23,25
|yg,29,22,31,22$29,23,31,23,29,25,31,25
|gg,31,22,29,22$29,23,31,23,29,25,31,25
]]
mail_data=[[
17,5;news outlet;whew!,that was a close call!,,there have been many floods,and landslides lately...,,local scientists are,baffled.
|9,29;gem shoppe;it seems gem sockets have,surfaced on your farm!,,how wonderful!,,gems will boost nearby,resources!
|5,21;mom;how is life on farmyfarm?!,,,,eat your vegetables!,,,,and clean behind your ears!
|99;gem shoppe;re: red gem findings,,fascinating... the red gem,combines the qualities of,yellow and green gems!,,it also appears to be,self-replicating!,,...how can this be possible?!,,keep a close eye on that red,gem!
|98;mysterious writer;yessssss... yesssssss!!,,my plan is working!,just keep using that red,gem. soon i will have,all the life force i need,to unlock my prison!,,ha ha ha ha ha ha!,,you are such a helpful little,farmer!,,farmyfarm world will be mine!,,ha ha ha ha ha ha!
|97;occult horticultuary;no!! what have you done?!,red gems will set HIM free!,,you must destroy the gems!,you will need the mystic,watering can.,,and we know where it is!,we sense it is on the back,of the giant sea turtle!,we can summon the turtle,to farmyfarm. hurry!,,retrieve the watering can or,all is lost!
|96;mysterious writer;noooooooooooooooooooOOOOOO...,,...*deep breath*...OOOOOOOOO,,*dies*
|95;occult horticultuary;you did it!,,farmyfarm is saved!,,you can keep the watering,can.,,it has special properties,that will help you in your,labors.,,it can restore damaged,producers and double the,growth rate (only once,though).
|94;mysterious writer;i see you found my gem!,,ha ha ha!,,i have a proposal for you.,,help your gem friends with,their testing of the red,gem. i suspect the results,will be... earth-shattering.,,ha ha ha...,,i'll be in touch...
]]
