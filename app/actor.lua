function actor:new(sprite_id,x,y,name,frm_inc,frms)
    local a={
        sprite_id=tonum(sprite_id),
        x=x,
        y=y,
        name=name,
        label=name,
        dx=0,
        dy=0,
        frm=0,
        frms=frms or 2,
        friction=1,
        w=0.4,
        h=0.4,
        frm_inc=frm_inc,
        tm=time(),
    }
    make_obj(a,self)
    return a
end
function actor:move()
    if self.spr_type!='prd' and self.name!='frc' and self.name!='title' and self.name!='bird' then
        if next_is_solid(self,self.dx,0) then
            self.dx*=-0.1
        end
        if next_is_solid(self,0,self.dy) then
            self.dy*=-0.1
        end
    end

    self.x+=self.dx
    self.y+=self.dy

    self.frm+=abs(self.dx)
    self.frm+=abs(self.dy)

    if(self!=plr)self.frm+=self.frm_inc or 0.1
    self.frm%=self.frms

    if self.friction!=0 then
        self.dx*=(0.65)
        self.dy*=(0.65)
    end

    if abs(self.dx)<0.001 and abs(self.dy)<0.001 then
        self.dx,self.dy=0,0
        if(not contains(ld_ids('pcoin,title,mail'),self.name))self.frm=0
    end
end
function actor:draw()
    if(self.name=='mail' and not mail.unread)return
    local sx,sy,clr=(self.x*8-4),(self.y*8-4),12
    if(self.selected)clr=10
    if(self.bkng or false)draw_bkng(sx+1,sy+1,self.bck_w or 7,self.bck_h or 1,clr)
    if self.name=='frc' and rnd(1)<0.05 then
        line(self.x*8+4,self.y*8+4,self.xt*8,self.yt*8,8)
        return
    end
    spr(self.sprite_id+self.frm,sx,sy,1,1,self.flip_x or false)
    if(contains(ld_ids('qst_itm,inv_itm'),self.label))print(self.count,sx,sy+9,self.cnt_clr or 7)
    if(self.name=='pcoin')showtext(coins,sx+20,sy-10)
end
