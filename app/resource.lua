function grow(a) a:grow() end
function update_creature(a) a:update() a:move() end
function draw_resource(a) a:draw_hp() end
function resource:new(sprite_id,x,y,name)
    local o=actor:new(sprite_id,x,y,name,0.15)
    make_obj(o,self)
    o.growing,o.ready,o.hits,o.hp=false,true,0,1
    o.base_hp,o.show_hp,o.show_hp_tm,o.proc_spr=1,false,-3,{}
    o.start_id,o.plr_trigger=sprite_id,false
    o.grow_spr=crop_grow_ids[sprite_id]
    o.grow_tick=0
    o.draw=actor.draw
    o.move=actor.move
    o.pause_tm=time()
    o.xa,o.ya=x,y
    o.gint=0
    o.fruit=false
    o.rg_trg=0
    o.gm_int=1
    o.bst=false
    o.yg,o.gg,o.rg=false,false,false
    if(o.grow_spr!=nil)o.fruit_id=o.grow_spr[#o.grow_spr]
    if(contains(multi_hp_crops,sprite_id))o.base_hp=3
    if(contains(fruit_crops,sprite_id))o.fruit=true
    if(contains(crtr_map,sprite_id))o.animal=true
    return o
end
function resource:grow()
    local div=(self.gg or self.rg) and 2 or 1
    if(self.bst)div*=2
    if self.growing and not self.absorbed and (game_state<2 or chktm(self.grow_tick)>self.gint/div) then
        local gspr=self.grow_spr
        local next_id=self:increment(gspr)
        if next_id==gspr[1] then
            self:prep_harv()
            self.growing=false
            if(self.fruit)self.hp=1
        elseif next_id==gspr[#gspr] and self.fruit then
            self:prep_harv()
        end
        self.hits=0
    end
    self:check_hp()
    if(self.yg or self.rg or false)self:gem_trigger()
end
function resource:gem_trigger()
    if(not contains(res_ids,self.sprite_id)or self.growing or self.absorbed) return
    if(chktm(self.gm_trg_tm or 0)<self.gm_int)return
    self.gm_trg_tm=time()
    if(self.rg)self.rg_trg+=1
    if(self.rg_trg>15) and not self.absorbed and red[2] and not red[3] then
        trigger_absorb(self)
        return
    end
    local prd_name=prd_map[self.sprite_id] or ''
    _,itm=inv_itm:get(prd_name)
    if(#prds<200)self:process(true)
end
function resource:prep_harv()
    self.ready=true
    self.harvested=false
    self.hp=self.base_hp
end
function resource:draw_hp()
    local x,y=self.x*8,(self.y-0.5)*8
    if self.rg then
        spr(172,x,y)
    else
        if(self.yg)spr(156,x,y)
        if(self.gg)spr(157,x,y)
    end
    if(self.bst)spr(123,x,y)
    if(self.show_hp or false)spr(140+self.hits,x,y-0.5)
end
function resource:increment(sprites)
    self.grow_tick=time()
    self.sprite_id=getnext(self.sprite_id,sprites)
    setm(self)
    return getnext(self.sprite_id,sprites)
end
function resource:process(gem)
    if(self.absorbed) return
    if not self.growing and not self.ready then
        sfx(50)
        local next_id=self:increment(self.proc_spr)
        local toolsprs=ld_ids('101,102,103|250,251,'..can_spr)
        tool_spr=toolsprs[self.sprite_id]
        dr_tl_cnt=(tool_spr!=nil and gem==nil) and 0 or 20
        if(next_id==self.proc_spr[1])self.growing=true self.ready=false return
    end
    if self.ready then
        self.hits+=1
        if(gem==nil)tool_spr,dr_tl_cnt=249,0
        self.show_hp_tm=time()
        if((self.sprite_id==self.fruit_id-1 and self.fruit) or (not self.fruit and self.base_hp==3))tool_spr=233 sfx(32)else sfx(51)
        if(self.hp==self.hits and not self.harvested)self:harvest()
    end
end
function resource:check_hp()
    self.show_hp=(self.hp!=nil and self.hp>1 and chktm(self.show_hp_tm)<2)
    if not self.show_hp then
        if(not self.fruit)self.hp=self.base_hp
        self.hits=0
    end
end
function resource:harvest()
    self.hits=0
    local x,y=getvec(self)
    make_prd(self.sprite_id,x+0.5,y+0.5)
    sfx(51)
    if self.fruit and self.sprite_id==self.fruit_id then
        self.sprite_id,self.growing,self.ready,self.hp=self.fruit_id-1,true,true,self.base_hp
        self:check_hp()
    else
        self.sprite_id,self.ready=self.start_id,false
        if(not self.plr_trigger)self.growing,self.hp=true,1
    end
    self.show_hp_tm,self.show_hp,self.grow_tick=-2,false,time()
    setm(self)
    self:set_grow_int()
end
function resource:update()
    if(not self.animal)return
    local tm=chktm(self.pause_tm)
    if tm<0.5 and self.xt!=nil then
        set_tgt_dir(self)
    elseif tm>5 then
        self:stop()
    end
end
function resource:stop()
    self.xt,self.yt=self.xa+rrng(10,-5),self.ya+rrng(10,-5)
    self.dx,self.dy,self.pause_tm=0,0,time()-rnd(1)
end
function resource:set_grow_int()
    self.gint=flr(rrng(15,15*1.5))
end
function generate_resources()
    for ea in all(res_map_crds) do
        make_resource(ea.sprite_id,ea.x,ea.y,ea.name)
    end
    for ea in all(crtr_cells) do
        make_creature(ea.sprite_id,ea.x,ea.y,crtr_map[ea.sprite_id])
    end
    parse_map_cells()
end
function make_resource(sprite_id,x,y,name)
    local rsrc=resource:new(sprite_id,x,y,name)
    rsrc.ld_id=sprite_id reset_res(rsrc,sprite_id) add(resources,rsrc)
    res_crds[prpos(x,y)]=rsrc rsrc:set_grow_int()
    if(red[3])rsrc.bst=true
end
function reset_res(res,spr_id)
    if contains(crop_end_ids,spr_id) then
        res.sprite_id=crop_start_id
        res.start_id=crop_start_id
        res.proc_spr=crop_proc_ids
        res.plr_trigger=true
    else
        res.start_id=res.grow_spr[1]
        res.sprite_id=res.start_id
        res.growing=true
    end
    setm(res)
    res.ready=false
end
function make_creature(sprite_id,x,y,name)
    local crtr=resource:new(sprite_id,x,y,name)
    add(creatures,crtr)
    crtr.friction,crtr.spd,crtr.w,crtr.h=0,0.001,0.6,0.6
    if(crtr.name=='bee' or crtr.name=='cat')crtr.spd=0.03 crtr.frm_inc=1
    if(crtr.name=='mouse' or crtr.name=='bird')crtr.spd=0.02
    if(crtr.name=='bird')setm(crtr,152)else setm(crtr,0)
end
function check_for_resource(x,y)
    local rsrc=res_crds[prpos(x,y)]
    if(rsrc==nil or contains(res_ids,rsrc.sprite_id))return rsrc
end
