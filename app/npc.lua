function update_npc(a) a:update() end
function npc:new(x,y,txt,name,spr_id,label)
    local o=actor:new(spr_id or 0,x,y,name or 'npc')
    o.label=label or 'npc'
    o.tgl_tm=0
    o.dialog=dialog:new(txt)
    make_obj(o,self)
    npc_map_crds[prpos(x,y)]=o
    add(npcs,o)
    return o
end
function npc:update()end
function npc:draw()
    self.dialog:draw()
    if self.name=='mail' and mail.unread then
        spr(158,self.x*8,(self.y*8-8)-sin(frm_ref/20))
    end
end
