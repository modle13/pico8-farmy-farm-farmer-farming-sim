function showtext(txt,x,y,clr)
    x=x or 64
    for i,v in pairs(split(txt)) do
        local yp=(y or 28)+12*i
        local endx=print(v,0,-100)
        if(v!='')draw_txt_bkng(x,yp,endx)
        print(v,x-endx/2,yp,clr or 7)
    end
end
function draw_txt_bkng(x,y,endx)
    local lft,rt=x-endx/2-1,x+endx/2
    rectfill(lft,y-1,rt+1,y+7,1)
    rectfill(lft-1,y-2,rt,y+6,12)
    rectfill(lft,y-1,rt-1,y+5,13)
end
function draw_bkng(x,y,endx,endy,clr)
    local yo=endy or 0
    rectfill(x-1,y-1,x+endx+1,y+7+yo,1)
    rectfill(x-2,y-2,x+endx,y+6+yo,clr or 12)
    rectfill(x-1,y-1,x+endx-1,y+5+yo,13)
end
function create_ui_sprites()
    add(ui_sprites,actor:new(80,2,1,'pcoin',0.2,4))
    local mail=actor:new(158,14,1,'mail',1.9)
    add(ui_sprites,mail)
end
function draw_confirm(x,y)
    local tspr=78
    if(frm_ref<10 or btn(4))tspr=79
    spr(tspr,x,y)
end
function draw_arrow(tspr,x,y)
    if(frm_ref<10)tspr+=1
    spr(tspr,x,y)
end
function show_title()
    showtext('farmyfarm,,,,,,,,,press start',64,0)
    spr(201,36,66)
    spr(215,60,66)
    spr(199,84,66)
    spr(202,36,64-sin(frm_ref/60)*2)
    spr(218,60,64-sin(frm_ref/60)*2)
    spr(200,84,64-sin(frm_ref/60)*2)
end
function draw_credits()
    showtext('thanks for playing',64,20)
    if win then
        draw_confirm(60,120)
        showtext('continue?',64,100)
    else
        showtext('(reset cart to play again)',64,80)
    end
end
function draw_end(txt)
    showtext(txt)
    draw_confirm(60,112)
    if(btnp(4))game_state=6
end
