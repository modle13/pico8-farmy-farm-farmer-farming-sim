function unlock:new(x,y,txt,name)
    local o=npc:new(x,y,txt,name)
    make_obj(o,self)
    return o
end
function create_unlocks()
    for ea in all(split(unlock_data,'|')) do
        local spl=split(ea)
        local x,y=spl[2],spl[3]
        local a=unlock:new(x,y,split(spl[4]..',,unlock?'),spl[4])
        a.cost,a.cat,a.xo,a.yo,a.label=spl[5],spl[6],spl[7] or 0,spl[8] or 0,'unlock'
        a.rpr=actor:new(spl[1],0,0,'')
    end
end
function unlock:process() if(not self.dialog.on) self.dialog:toggle(true) self.tgl_tm=time() end
function unlock:update()
    if(chktm(self.tgl_tm)<0.25 or self.cost>coins) return
    if(btnp(4) and self.dialog.txt!=nil and self.dialog.on)self:complete()
    if(btnp(5))self.dialog:toggle(false)
end
function unlock:complete(ld)
    tool_spr,dr_tl_cnt=249,0
    if(not self.dialog.on and not ld) return
    if(not contains(ld_ids('res,wall'),self.cat))return
    local sp=self.rpr.sprite_id
    local xo=self.x+self.xo
    local yo=self.y+self.yo
    if(self.cat=='res')make_resource(sp,xo,yo,self.name)
    if(self.cat=='wall')setm(self,0) mset(xo,yo,0)
    if(not ld)self:buy()
    sfx(31)
end
function unlock:buy()
    coins-=self.cost
    npc_map_crds[prpos(self.x,self.y)]=nil
    self.dialog.on=false
    del(npcs,self)
end
function unlock:draw()
    if(not self.dialog.on) return
    self.dialog:draw()
    local clr=7
    if(self.cost>coins)clr=8
    print(self.cost,self.dialog.x+76,self.dialog.y+8,clr)
    if(self.rpr!=nil)spr(self.rpr.sprite_id,self.dialog.x+64,self.dialog.y+8)
end
