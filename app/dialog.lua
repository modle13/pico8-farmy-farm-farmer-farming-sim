function dialog:new(txt)
    local o={
        on=false,
        txt=txt,
        x=4,
        y=16,
        cnt=0,
        button=true,
        fullread=false,
    }
    make_obj(o,self)
    return o
end
function dialog:toggle(st)
    if(st!=nil)self.on=st else self.on=not self.on
    if(not self.on)self.fullread=false
end
function dialog:draw()
    if(not self.on or self.txt==nil)self.cnt=0 return
    local h=(#self.txt+1)*6
    local clr=self.selected and 10 or 12
    draw_bkng(self.x+1,self.y+1,120,h,clr)
    if(self.button or false)draw_confirm(112,self.y+h)
    if(self.extr!=nil)spr(self.extr,108,self.y+8)
    local outstr=''
    for ea in all(self.txt) do
        outstr=outstr..'\n'..ea
    end
    self.cnt+=0.5
    local topr=sub(outstr,1,self.cnt)
    if(self.fullread)topr=outstr self.cnt=#outstr
    print(topr,8,self.y,7)
    if(self.cnt<#outstr and sub(topr,-1)!='\n')sfx(47)
end
