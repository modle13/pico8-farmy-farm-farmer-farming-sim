function sign:new(x,y,txt)
    local o=npc:new(x,y,txt)
    make_obj(o,self)
    return o
end
function sign:update()
    if(chktm(self.tgl_tm)<0.25) return
    if(not self.dialog.fullread and btnp(4) and self.tgl_tm>0.3)self.dialog.fullread=true self.tgl_tm=time() return
    if(btnp(4) or btnp(5))self.dialog:toggle(false) self.tgl_tm=0
end
function sign:process() if(self.dialog.on) return else self.dialog:toggle(true) self.tgl_tm=time() end
function sign:draw() self.dialog:draw() end
function make_signs()
    for ea in all(split(sign_data,'|')) do
        local spl=split(ea)
        local x,y=spl[1],spl[2]
        sign:new(x,y,split(spl[3],'$'))
    end
end
