sed -i 's/debug=true/debug=false/g' app/state.lua
pico8 -export farmyfarm.html farmyfarm.p8
mv farmyfarm.html index.html
zip farmyfarm.zip index.html farmyfarm.js
pico8 -export farmyfarm.p8.png farmyfarm.p8
rm index.html
rm farmyfarm.js
