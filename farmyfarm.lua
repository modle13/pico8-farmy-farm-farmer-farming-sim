-- title: Farmyfarm
-- author: modle
-- description:
--     farming simulator

game_state=0
win=false
spot=nil
m_sw_tm=0
m_sw_del=90

function _init()
    plr=actor:new(64,1.5,1.5,'player',nil,4)
    plr.w,plr.h=0.2,0.2

    inv_gem=inv:new('inv_gem')
    inv_gem.x=15
    inv_itm=inv:new()
    mail={data={},name='mailbox',on=false,unread=false,dialog=dialog:new({})}
    qsts=quest:new()
    shop=make_shop()

    sw_msc(7)

    create_ui_sprites()

    parse_map_cells()
    generate_resources()
    make_mailboxes()
    ld_mail()
    make_signs()
    make_gems()
    create_unlocks()

    game_state=1
end

function _update()
    manage_frm()
    update_reference_sprites()
    foreach(wave_cells,update_map_cells)
    foreach(resources,grow)

    hndl_music()

    if game_state==1 then
        local a=actor:new(rnd(split('64,68,70,84,86,240,242,220,236,228,212,168,214')),rnd(split('-2,17')),rnd(17),'title')
        a.dx,a.dy,a.friction=rrng(0.5,-0.25),rrng(0.5,-0.25),0
        if(a.sprite_id==64)a.toolspr=rnd(split('233,249,250,251,252'))
        if(a.dx<0)a.flip_x=true
        add(title_spr,a)return
    end
    title_spr={}

    if not spot then
        empty_cells=get_map_cells('empty',{[0]=1},33,26)
        spot=rnd(empty_cells)
        if spot.y>5 then
            mset(spot.x,spot.y,197)
            parse_map_cells()
        end
    end

    check_menu_ops()

    control_player()
    inv_itm:update()
    mail_update()
    qsts:update()
    shop:update()

    foreach(creatures,update_creature)

    update_events()
    foreach(npcs,update_npc)
    foreach(prds,update_prd)
    foreach(ui_sprites,move_actor)
end

map_crabs=false

function _draw()
    cls()

    if game_state==1 then
        map(80,0,0,0,16,16)
        for ea in all(title_spr) do
            ea:move()ea:draw()if(ea.x<-4 or ea.x>20 or ea.y<-4 or ea.y>20) del(title_spr,ea)
            if(ea.sprite_id==64)spr(ea.toolspr,(ea.x+(ea.flip_x and -1.5 or 0))*8+2,ea.y*8-4,1,1,ea.flip_x)
        end
        palt(0,false)map(96,0,0,0,16,16)palt(0,true)
        show_title()if(btn(6)) poke(0x5f30,1) game_state+=1 return
    end
    if game_state==6 then
        draw_credits()
        if(win and btnp(4))game_state=2
        return
    end
    if game_state==4 then
        draw_win() sw_msc(7)return
    end
    if game_state==5 then
        map(96,16,0,0,16,16)
        draw_end(",,the crops of chaos,have been unleashed...,the world has descended,into darkness!")
        return
    end
    if(game_state!=2)return

    update_cam()
    map(0,0,0,0,64,32)
    if(mget(43,7)==0)map(64,0,296,24,12,11)

    foreach(creatures,draw_actor)
    foreach(misc_actors,draw_actor)
    foreach(prds,draw_actor)
    foreach(resources,draw_resource)
    draw_tool()
    foreach(gems,draw_actor)
    foreach(mailboxes,draw_actor)
    plr:draw()

    camera()

    if(plr.x>55)sw_msc(0)
    if corrupted>0 then
        if(rnd(1)<0.002)send_mail(98)
        sw_msc(0)
        rectfill(48,8,80,12,1)rectfill(49,9,min(79,49+corrupted*2+2),11,8)
        if not red[3] and go_tm>0 then
            showtext(180-flr(time()-go_tm),82+rrng(1,0.5),-4,8)
            if(rnd(1)<0.002)sfx(55)
            if(time()-go_tm>180)game_state=5
        end
    else
        if(red[3] and game_state<4 and not win)game_state=4
    end
    inv_gem:draw()
    inv_itm:draw()
    mail_draw()
    qsts:draw()
    shop:draw()
    if(cur_npc!=nil and not contains(ld_ids('box,mail,gem'),cur_npc.name))cur_npc:draw()
    foreach(ui_sprites,draw_actor)
end

function draw_win()
    win=true
    if((frm_ref==30 or frm_ref==0)and map_crabs)map_crabs=false
    if not map_crabs then
        for x=112,127 do
            for y=0,4 do
                mset(x,y,0)
                for ea in all(wave_cells) do
                    if(ea.x==x and ea.y==y)del(wave_cells,ea)
                end
            end
        end
        if frm_ref<30 then
            rmap("114,0,168|116,0,168|118,0,168|119,0,168|120,0,168|122,0,168|124,0,168|115,1,168|118,1,168|120,1,168|122,1,168|124,1,168|115,2,168|118,2,168|120,2,168|122,2,168|124,2,168|115,3,168|118,3,168|119,3,168|120,3,168|122,3,168|123,3,168|124,3,168")
        else
            rmap("112,0,168|113,0,168|114,0,168|116,0,168|117,0,168|118,0,168|120,0,168|121,0,168|122,0,168|124,0,168|126,0,168|112,1,168|114,1,168|116,1,168|118,1,168|120,1,168|124,1,168|125,1,168|112,2,168|113,2,168|116,2,168|118,2,168|120,2,168|124,2,168|125,2,168|112,3,168|114,3,168|116,3,168|117,3,168|118,3,168|120,3,168|121,3,168|122,3,168|124,3,168|126,3,168")
        end
        parse_map_cells()
        map_crabs=true
    end
    map(112,0,0,0,16,16)
    draw_end(",,the crops of chaos,have been quelled...,all hail the hero,of farmyfarm!")
end

function hndl_music()
    if corrupted>0 or plr.x>55 then
        return
    else
        if(trk==0)sw_msc(2)
    end
    if(time()-m_sw_tm>m_sw_del)sw_msc(rnd(split('2,7'))) m_sw_tm=time()
end
